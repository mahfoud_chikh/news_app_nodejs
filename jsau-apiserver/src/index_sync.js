let express = require('express')
let app = express()
let helpers = require('./helpers.js')

const fs = require('fs')
let path = require('path')
let bodyParser = require('body-parser')

app.use(bodyParser.json())

app.listen(8080, () => {
    console.log('listening on 8080')
})


////////////////////////// Logic /////////////////////////

let jsonFilePath = path.join(__dirname, '../data/news.json')


// Get all news
app.get('/news', (req, res) => {

    helpers.handleHttpError(res, () => {

        const result = _getAllNews()

        res.json({
            news:result,
            error : null
        })

    })

})

const _getAllNews = () => {

    let oldNewsJSON = fs.readFileSync(jsonFilePath, 'utf8')

    let news = JSON.parse(oldNewsJSON)

    if (!news || news.length == 0) {
        throw new Error('404')
    }

    return news

}


// Get news by id
app.get('/news/:id', (req, res) => {

    const id = parseInt(req.params.id)

    helpers.handleHttpError(res, () => {
        const result = _getNewsById(id)

        res.json({
            news:result,
            error : null
        })

    })

})


const _getNewsById = (id) => {

    if (!id) {
        throw new Error('404')
    }

    let oldNewsJSON = fs.readFileSync(jsonFilePath, 'utf8')

    let news = JSON.parse(oldNewsJSON)

    const result = news.filter((n) => n.id == id)[0]

    if (!result) {
        throw new Error('404')
    }

    return result

}


// add news
app.post('/news', (req, res) => {

    const newNews = req.body

    helpers.handleHttpError(res, () => {
        const result = _postNews(newNews)

        res.status(201).json({
            message: 'created with id ' + result,
            error : null
        })

    })

})


const _postNews = (news) => {
    if (!news) {
        throw new Error('400')
    }

    // check body content
    let oldNewsJSON = fs.readFileSync(jsonFilePath, 'utf8')
    let oldNews = JSON.parse(oldNewsJSON)

    // check file content
    if (!Array.isArray(oldNews)) {
        oldNews = []
    }

    let lastID = !oldNews.length ? 1 : oldNews[oldNews.length - 1]['id'] + 1

    const extNewNews = {
        id : lastID,
        ...news
    }

    const totalNews = [...oldNews, extNewNews]

    fs.writeFile(jsonFilePath, JSON.stringify(totalNews), (err) => {
        if (err) {
            throw err
        }
    })

    return lastID

}


// Update News
app.put('/news/:id', (req, res) => {

    const newsId = parseInt(req.params.id)
    const content = req.body

    helpers.handleHttpError(res, () =>{

        _updateNews(newsId, content)

        res.json({
            error : null
        })

    })

})


const _updateNews = (id, content) => {

    if (!id || !content) {
        throw new Error('400')
    }


    let oldNewsJSON = fs.readFileSync(jsonFilePath, 'utf8')
    let oldNews = JSON.parse(oldNewsJSON)

    // check file content
    if (!Array.isArray(oldNews)) {
        oldNews = []
    }

    let newsExists = false

    const newNews = oldNews.map(
        (news) => {
            if (news.id == id) {
                newsExists = true
                return {
                    id,
                    ...content
                }
            }
            return news
        })

    if (!newsExists) {
        throw new Error('404')
    }

    fs.writeFile(jsonFilePath, JSON.stringify(newNews), (err) => {
        if (err) {
            throw err
        }
    })

}


// Delete News
app.delete('/news/:id', (req, res) => {

    const newsId = parseInt(req.params.id)

    helpers.handleHttpError(res, () => {

        _deleteNews(newsId)

        res.json({
            error : null
        })

    })

})


const _deleteNews = (id) => {

    if (!id) {
        throw new Error('400')
    }

    const oldNewsJSON = fs.readFileSync(jsonFilePath, 'utf8')

    let oldNews = JSON.parse(oldNewsJSON)

    // check file content
    if (!Array.isArray(oldNews)) {
        oldNews = []
    }

    const index = oldNews.findIndex((news) => news.id == id)

    if (0 > index) {
        throw new Error('404')
    }

    // delete 1 item at index
    oldNews.splice(index, 1)

    fs.writeFile(jsonFilePath, JSON.stringify(oldNews), (err) => {
        if (err) {
            throw err
        }
    })

}
