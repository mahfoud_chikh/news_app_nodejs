let express = require('express')
let app = express()
let helpers = require('./helpers.js')

const fs = require('fs')
let path = require('path')
let bodyParser = require('body-parser')

app.use(bodyParser.json())


app.listen(8888, () => {
    // eslint-disable-next-line no-console
    console.log('listening on 8888')
})


app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8080') // update to match the domain you will make the request from
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

////////////////////////// Logic /////////////////////////

let jsonFilePath = path.join(__dirname, '../data/news.json')


// Get all news
app.get('/news', (req, res) => {

    _getAllNews({
        onSuccess: (news) => helpers.succesHttpResponse(res, {news}),
        onError: (err) => helpers.handleHttpError(err, res)
    }
    )
})

const _getAllNews = ({onSuccess, onError}) => {

    fs.readFile(jsonFilePath, (err, data) => {

        if (err) {
            return onError(err)
        }

        let news = JSON.parse(data)

        return onSuccess(news)

    })

}


// Get news by id
app.get('/news/:id', (req, res) => {

    const id = parseInt(req.params.id)

    _getNewsById(id,
        {
            onSuccess: (news) => helpers.succesHttpResponse(res, {news}),
            onError: (err) => helpers.handleHttpError(err, res)
        }
    )

})


const _getNewsById = (id, {onSuccess, onError}) => {

    if (!id) {
        return onError(new Error('404'))
    }

    fs.readFile(jsonFilePath, (err, data) => {

        if (err) {
            return onError(err)
        }

        let news = JSON.parse(data)

        const result = news.filter((n) => n.id == id)[0]

        return onSuccess(result)

    })

}


// add news
app.post('/news', (req, res) => {

    const newNews = req.body

    _postNews(newNews,
        {
            onSuccess:
          (id) => helpers.succesHttpResponse(res, {last_id : id}, 201),
            onError: (err) => helpers.handleHttpError(err, res)
        }
    )

})


const _postNews = (news, {onSuccess, onError}) => {

    // check body content
    if (!news) {
        return onError(new Error('400'))
    }

    fs.readFile(jsonFilePath, (err, data) => {

        if (err) {
            return onError(err)
        }

        let oldNews = JSON.parse(data)

        // check file content
        if (!Array.isArray(oldNews)) {
            oldNews = []
        }

        let lastID = !oldNews.length ? 1 : oldNews[oldNews.length - 1]['id'] + 1

        const extNewNews = {
            id : lastID,
            ...news
        }

        const totalNews = [...oldNews, extNewNews]

        fs.writeFile(jsonFilePath, JSON.stringify(totalNews), (err) => {

            if (err) {
                return onError(err)
            }

            return onSuccess(lastID)

        })

    })

}


// Update News
app.put('/news/:id', (req, res) => {

    const newsId = parseInt(req.params.id)
    const content = req.body

    _updateNews(newsId, content,
        {
            onSuccess: () => helpers.succesHttpResponse(res),
            onError: (err) => helpers.handleHttpError(err, res)
        }
    )

})


const _updateNews = (id, content, {onSuccess, onError}) => {

    if (!id) {
        return onError(new Error('404'))
    }
    if (!content) {
        return onError(new Error('400'))
    }

    fs.readFile(jsonFilePath, (err, data) => {

        if (err) {
            return onError(err)
        }

        let oldNews = JSON.parse(data)

        // check file content
        if (!Array.isArray(oldNews)) {
            oldNews = []
        }

        let newsExists = false

        const newNews = oldNews.map(
            (news) => {
                if (news.id == id) {
                    newsExists = true
                    return {
                        id,
                        ...content
                    }
                }
                return news
            })

        if (!newsExists) {
            return onError(new Error('404'))
        }

        fs.writeFile(jsonFilePath, JSON.stringify(newNews), (err) => {

            if (err) {
                return onError(err)
            }

            return onSuccess()

        })

    })
}


// Delete News
app.delete('/news/:id', (req, res) => {

    const newsId = parseInt(req.params.id)

    _deleteNews(newsId, {
        onSuccess: () => helpers.succesHttpResponse(res),
        onError: (err) => helpers.handleHttpError(err, res)
    }
    )

})


const _deleteNews = (id, {onSuccess, onError}) => {

    if (!id) {
        return onError(new Error('404'))
    }

    fs.readFile(jsonFilePath, (err, data) => {

        if (err) {
            return onError(err)
        }

        let oldNews = JSON.parse(data)

        // check file content
        if (!Array.isArray(oldNews)) {
            oldNews = []
        }

        const index = oldNews.findIndex((news) => news.id == id)

        if (index < 0) {
            return onError(new Error('404'))
        }

        // delete 1 item at index
        oldNews.splice(index, 1)

        fs.writeFile(jsonFilePath, JSON.stringify(oldNews), (err) => {

            if (err) {
                return onError(err)
            }

            return onSuccess()

        })

    })

}
