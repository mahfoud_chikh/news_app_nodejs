
module.exports.handleHttpError = (response, callback) => {
    try {
        callback()
    } catch (e) {
        switch (e.message) {
        case '404':
            console.log('400000000000000000000000000000000000000004')
            response.status(404).json({
                error : 'Not Found'
            })
            break
        case '400':
            response.status(400).json({
                error : 'Bad Input'
            })
            break
        default:
            console.log(e)
            response.status(500).json({
                error : 'Internal error'
            })

        }
    }
}


module.exports.succesHttpResponse = (response, result, status = 200) => {
    response.status(status).json({
        ...result,
        error : null
    })
}
