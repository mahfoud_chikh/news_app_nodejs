import AppContactLink from '../web-component/app-contact-link.vue'
import {AxiosInstance as axios} from "axios";

export default {
    // Local component registration
    components: {
        AppContactLink
    },
    data() {
        return {
            date: new Date(),
            tada: 'Some fixed text',
            pane_text: '',
            news: []
        }
    },
    mounted() {
        fetch('http://localhost:8888/news')
            .then((response) => {
                return response.json()
            })
            .then((data) => this.news = data.news)
            .catch((err) => {
                console.log(err)
            });

    },
    methods: {
        doIt(evt) {
            this.pane_text = this.pane_text + 'Do. Or do not. There is no try. '

            console.log(this.news)
        }
    }
}
