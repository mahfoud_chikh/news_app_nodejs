const express = require('express')
const app = express()
const helpers = require('./helpers.js')
const nunjucks = require('nunjucks')
const fs = require('fs')
const jsauNewsUtils = require('jsau-news-utils');

const path = require('path')

const bodyParser = require('body-parser')
const methodOverride = require('method-override')

nunjucks.configure([path.join(__dirname, 'views/')], {
    autoescape: true,
    express: app
})


app.use('/scripts', express.static(path.join(__dirname, '../node_modules/toastr/build/')))

app.use(bodyParser.urlencoded({extended: true}))
app.use(methodOverride('_method'))


app.listen(8080, () => {
    // eslint-disable-next-line no-console
    console.log('listening on 8080')
})


app.get('/info', (req, res) => {

    res.render('info.njk', {appVersion: 'jsau-webserver-1.0.0'})
})

app.get('/news', (req, res) => {

    _getAllNews({
        onSuccess: (news) => res.render('front-office/home/home.njk', {news}),
        onError: (err) => helpers.handleHttpError(err, res)
    })
})

app.get('/admin', (req, res) => {

    _getAllNews({
            onSuccess: (news) => res.render('back-office/home/home.njk', {news}),
            onError: (err) => helpers.handleHttpError(err, res)
        }
    )

})


app.get('/news/add', (req, res) => {

    const categories = jsauNewsUtils.categories;

    res.render('back-office/news/add.njk', {categories})

})

// Get news by id
app.get('/news/:id', (req, res) => {

    const id = parseInt(req.params.id)

    _getNewsById(id,
        {
            onSuccess: (news) => res.render('front-office/news/detail.njk', {news}),
            onError: (err) => helpers.handleHttpError(err, res)
        }
    )

})

// add news
app.post('/news',
    (req, res) => {
        const errs = jsauNewsUtils.validate(req.body);
        res.locals.oldValues = req.body;
        if (errs) {
            res.render('back-office/news/add.njk',
                {
                    errors: errs
                }
            );
            return
        }

        const newNews = req.body;


        _postNews(newNews,
            {
                onSuccess: (id) => helpers.succesHttpResponseView(res, {last_id: id}, '/admin', 201),
                onError: (err) => helpers.handleHttpError(err, res)
            }
        )

    })


// update news
app.get('/news/update/:id', (req, res) => {

    const id = parseInt(req.params.id);
    const categories = jsauNewsUtils.categories;

    _getNewsById(id,
        {
            onSuccess: (news) => res.render('back-office/news/update.njk', {news, categories}),
            onError: (err) => helpers.handleHttpError(err, res)
        }
    )

})

app.put('/news/:id', (req, res) => {
    const errs = jsauNewsUtils.validate(req.body);
    res.locals.oldValues = req.body;
    if (errs) {
        res.render('back-office/news/update.njk',
            {
                errors: errs

            }
        );
        return
    }

    const newsId = parseInt(req.params.id);
    const content = req.body;

    _updateNews(newsId, content,
        {
            onSuccess: () => helpers.succesHttpResponseView(res, null, '/admin'),
            onError: (err) => helpers.handleHttpError(err, res)
        }
    )

})

// Delete News
app.delete('/news/:id', (req, res) => {

    const newsId = parseInt(req.params.id)

    _deleteNews(newsId, {
        onSuccess: () => helpers.succesHttpResponse(res),
        onError: (err) => helpers.handleHttpError(err, res)
    })

})


////////////////////////// Logic /////////////////////////

// data file path
let jsonFilePath = path.join(__dirname, '../data/news.json')


const _getAllNews = ({onSuccess, onError}) => {

    fs.readFile(jsonFilePath, (err, data) => {

        if (err) {
            return onError(err)
        }

        let news = JSON.parse(data)

        return onSuccess(news)

    })

}


const _getNewsById = (id, {onSuccess, onError}) => {

    if (!id) {
        return onError(new Error('404'))
    }

    fs.readFile(jsonFilePath, (err, data) => {

        if (err) {
            return onError(err)
        }

        let news = JSON.parse(data)

        const result = news.filter((n) => n.id == id)[0]

        return onSuccess(result)

    })

}


const _postNews = (news, {onSuccess, onError}) => {

    console.log(news)
    // return onError(new Error('err'))

    // check body content
    if (!news) {
        return onError(new Error('400'))
    }

    fs.readFile(jsonFilePath, (err, data) => {

        if (err) {
            return onError(err)
        }

        let oldNews = JSON.parse(data);

        // check file content
        if (!Array.isArray(oldNews)) {
            oldNews = []
        }

        let lastID = !oldNews.length ? 1 : oldNews[oldNews.length - 1]['id'] + 1

        const extNewNews = {
            id: lastID,
            ...news
        }

        const totalNews = [...oldNews, extNewNews]

        fs.writeFile(jsonFilePath, JSON.stringify(totalNews), (err) => {

            if (err) {
                return onError(err)
            }

            return onSuccess(lastID)

        })

    })

}


const _updateNews = (id, content, {onSuccess, onError}) => {

    if (!id) {
        return onError(new Error('404'))
    }
    if (!content) {
        return onError(new Error('400'))
    }

    fs.readFile(jsonFilePath, (err, data) => {

        if (err) {
            return onError(err)
        }

        let oldNews = JSON.parse(data);

        // check file content
        if (!Array.isArray(oldNews)) {
            oldNews = []
        }

        let newsExists = false;

        const newNews = oldNews.map(
            (news) => {
                if (news.id == id) {
                    newsExists = true;
                    return {
                        id,
                        ...content
                    }
                }
                return news
            })

        if (!newsExists) {
            return onError(new Error('404'))
        }

        fs.writeFile(jsonFilePath, JSON.stringify(newNews), (err) => {

            if (err) {
                return onError(err)
            }

            return onSuccess()

        })

    })
}


const _deleteNews = (id, {onSuccess, onError}) => {

    if (!id) {
        return onError(new Error('404'))
    }

    fs.readFile(jsonFilePath, (err, data) => {

        if (err) {
            return onError(err)
        }

        let oldNews = JSON.parse(data)

        // check file content
        if (!Array.isArray(oldNews)) {
            oldNews = []
        }

        const index = oldNews.findIndex((news) => news.id == id)

        if (index < 0) {
            return onError(new Error('404'))
        }

        // delete 1 item at index
        oldNews.splice(index, 1)

        fs.writeFile(jsonFilePath, JSON.stringify(oldNews), (err) => {

            if (err) {
                return onError(err)
            }

            return onSuccess()

        })

    })

}
